# ApiLibrary



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## How to use
get this code with the following command:

```
cd `directory folder`
git clone https://gitlab.com/MadeBy-Herianto/library.git
```

make environment on w with the following command by tutorial in https://flask.palletsprojects.com/en/3.0.x/installation/#virtual-environments , example in windows:

```
py -3 -m venv .venv
.venv/Scripts/activate
```

install all reqruitments library with the following command:

```
pip install -r requirements.txt
```

make empty database 'library4' in your postgress DBMS, then flask-migrate database tructure by following models Flask-SQLAlchemy in code with the following command:

```
flask db init
flask db migrate -m 'message migrate'
flask upgrade
```

now, you can use the Apilibray with the following command:

```
flask --app app run
```