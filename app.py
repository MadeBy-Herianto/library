from flask import jsonify, request
from pecahan_code.models import *
from nanoid import generate
from pecahan_code.auth import *
from datetime import datetime

print(datetime.now())
@app.route("/")
def all() :
	return 'this library api'

# mendapatkan/mengambil data semua members  
@app.route("/<version>/members/") 
def Members_func(version):
	if (version in ['v1']):
		auth = request.headers.get('Authorization')
		Author = authorization(auth)
		if Author['permission']:
			return [{
				'id' : x.member_id,
				'name' : x.name
			}for x in Members.query.all()]
		return jsonify({
			'error': 'authorized',
			'message' : Author['message']
		}), 401

@app.route("/<version>/member/", methods=['POST', 'GET', 'PUT']) 
def Member_func(version):
	auth = request.headers.get('Authorization')

	# menambahkan data member oleh admin
	if request.method == 'POST' and (version in ['v1']):
		Author = authorization(auth,permission=['admin'])
		if Author['permission']:
			data = request.get_json()
			#perlu di tambahkan validator untuk request json
			u = Members(
					member_id = 'M-' + generate(size=7),
					name = 'Member-' + data['username'],
					password = convert_authorization_password(data['password'])
				)
			db.session.add(u)
			db.session.commit()
			return ({
				"status":"success",
				"member_id" : u.member_id
			}), 201
		return jsonify({
			'error' : 'authorized',
			'message' : Author['message']
		}), 401
	
	# mendapatkan/mengambil data(id,name) member oleh member 
	if request.method == 'GET' and (version in ['v1']):
		Author = authorization(auth ,permission=['member'])
		if Author['permission']:
			return jsonify({
				'id' : Author['id'],
				'username' : Author['username'],
				'user_type': Author['user_type']
			})
		return jsonify({
			'error': 'authorized',
			'message' : Author['message']
		}), 401
	
	# mengganti data(password) member oleh member
	if request.method == 'PUT' and (version in ['v1']):
		Author = authorization(auth,permission=['member'])
		if Author['permission']:
			data = request.get_json()
			u = Members.query.filter_by(member_id=Author['id']).first()
			u.password=convert_authorization_password(data['new_password'])
			db.session.commit()
			return jsonify({
				'id' : Author['id'],
				'username' :Author['username'],
				'password' :{
					'change' : 'success', 
					'new_password' : u.password
				}
			})
		return jsonify({
			'error': 'authorized',
			'message' : Author['message']
		}), 401

# mendapatkan/mengambil data administrators
@app.route("/<version>/administrators/") 
def Administrators_func(version):
	auth = request.headers.get('Authorization')
	if (version in ['v1']):
		Author = authorization(auth, permission=['admin'])
		if Author['permission']:
			return [{
				'id' : x.admin_id,
				'name' : x.name
			}for x in Administrators.query.all()], 200
		return jsonify({
			'error': 'authorized',
			'message' : Author['message']
		}), 401

@app.route("/<version>/administrator/", methods=['POST', 'GET', 'PUT']) 
def Administrator_func(version):
	auth = request.headers.get('Authorization')
	
	# menambahkan data administrators
	####### perlu di tambahkan authentikasi untuk penanggug jawab, misalnnya kepala perpustakaan
	if request.method == 'POST' and (version in ['v1']):
		data = request.get_json()
		u = Administrators(
				admin_id = 'A-' + generate(size=7),
				name = 'Admin-' + data['username'],
				password = convert_authorization_password(data['password'])
			)
		db.session.add(u)
		db.session.commit()
		return ({
				"status":"success",
				"member_id" : u.admin_id
			}), 201

	# mendapatkan/mengambil data administrator
	if request.method == 'GET' and (version in ['v1']):
		Author = authorization(auth, permission=['admin'])
		if Author['permission']:
			return {
				'id' :Author['id'],
				'username' : Author['username'],
				'user_type' :Author['user_type']
			}, 200
		return jsonify({
			'error': 'authorized'
		}), 401
	
	# mengganti data(name, password) administrator oleh administrator
	if request.method == 'PUT' and (version in ['v1']):
		Author = authorization(auth,permission=['admin'])
		if Author['permission']:
			data = request.get_json()
			u = Administrators.query.filter_by(admin_id=Author['id']).first()
			u.password=convert_authorization_password(data['new_password'])
			db.session.commit()
			return jsonify({
				'id' : Author['id'],
				'username' :Author['username'],
				'password' :{
					'change' : 'success', 
					'new_password' : u.password
				}
			})
		return jsonify({
			'error': 'authorized',
			'message' : Author['message']
		}), 401

@app.route("/<version>/categories/", methods = ['GET','POST'])
def Categories_func(version):
	auth = request.headers.get('Authorization')

	# mendapatkan/mengambil data categories
	if request.method == 'GET' and (version in ['v1']):
		Author = authorization(auth)
		if Author['permission']:
			a = Administrators.query.all()
			return [{
				'name' : x.name,
				'description' : x.description,
				"category_id" : x.category_id 
			}for x in Categories.query.all()], 200
		return jsonify({
				'error': 'authorized',
				'message' : Author['message']
			}), 401
	
	# menambahkan data categories
	if request.method == 'POST' and (version in ['v1']):
		Author = authorization(auth, permission=['admin'])
		if Author['permission']:
			category = request.get_json()
			ctgr =[]
			for data in category:
				c = Categories(
					name = data['name'],
					description = data['description']
				)
				db.session.add(c)
				db.session.commit()
				added = {
					"category_id" : c.category_id,
					"name" : c.name
				}
				ctgr.append(added)
			return ({
					"status":"success",
					"category_added" : ctgr
				}), 201
		return jsonify({
				'error': 'authorized',
				'message' : Author['message']
			}), 401

@app.route("/<version>/category/<id>/", methods = ['PUT', 'DELETE'])
def Category_id_func(version, id):
	auth = request.headers.get('Authorization')
	# merubah data kategori
	if request.method == 'PUT' and (version in ['v1']):
		Author = authorization(auth, permission=['admin'])
		if Author['permission']:
			data = request.get_json()
			print(data)
			c = Categories.query.filter_by(category_id= id).first()
			result = {
				'category_id': c.category_id,
				'name' : c.name,
				'description' : c.description,
				'change_to' : {}
			}
			if 'name' in data:
				c.name = data['name']
				result['change_to']['name'] = data['name']
			if 'description' in data:
				c.description = data['description']
				result['change_to']['description'] = data['description']
			db.session.commit()
			return jsonify(result),201
		return jsonify({
				'error': 'authorized',
				'message' : Author['message']
			}), 401
			
	# menghapus data kategori
	if request.method == 'DELETE' and (version in ['v1']):
		Author = authorization(auth, permission=['admin'])
		if Author['permission']:
			c = Categories.query.filter_by(category_id= id).first()
			name = c.name
			db.session.delete(c)
			db.session.commit()
			return ({
						"status":"category deleted",
						"category_id" : id,
						"name" : name
					}), 201
		return jsonify({
				'error': 'authorized',
				'message' : Author['message']
			}), 401



@app.route("/<version>/writers/", methods = ['GET','POST'])
def Writers_func(version):
	auth = request.headers.get('Authorization')

	# mendapatkan/mengambil data writers 
	if request.method == 'GET' and (version in ['v1']):
		Author = authorization(auth)
		if Author['permission']:
			a = Administrators.query.all()
			return [{
				"writer_id" : x.writer_id,
				'name' : x.name,
				'citizen' : x.citizen,
				'year_of_birth' : x.year_of_birth
			}for x in Writers.query.all()], 200
		return jsonify({
				'error': 'authorized',
				'message' : Author['message']
			}), 401
	
	# menambahkan data writers
	if request.method == 'POST' and (version in ['v1']):
		auth = request.headers.get('Authorization')
		Author = authorization(auth, permission=['admin'])
		if Author['permission']:
			writer = request.get_json()
			wrtr =[]
			for data in writer:
				w = Writers(
					writer_id = 'w-' + generate(size=7),
					name = data['name'],
					citizen = data.get("citizen", None),
					year_of_birth = data.get("year_of_birth", None)
				)
				db.session.add(w)
				db.session.commit()
				added = {
					"writer_id" : w.writer_id,
					"name" : w.name
				}
				wrtr.append(added)
			return ({
					"status":"success",
					"writer_added" : wrtr
				}), 201
		return jsonify({
				'error': 'authorized',
				'message' : Author['message']
			}), 401
	return {
		'message' : 'Sorry, version not already to use'
	}, 500


@app.route("/<version>/writer/<id>/", methods = ['PUT', 'DELETE'])
def Writer_id_func(version, id):
	auth = request.headers.get('Authorization')
	if request.method == 'PUT' and (version in ['v1']):
		Author = authorization(auth, permission=['admin'])
		if Author['permission']:
			data = request.get_json()
			w = Writers.query.filter_by(writer_id= id).first()
			result = {
				'writer_id': w.writer_id,
				'name' : w.name,
				'citizen' : w.citizen,
				'year_of_birth' : w.year_of_birth,
				'change_to' : {}
			}
			if 'name' in data:
				result['change_to']['name'] = data['name']
			if 'citizen' in data:
				result['change_to']['citizen'] = data['citizen']
			if 'year_of_birth' in data:
				result['change_to']['year_of_birth'] = data['year_of_birth']
			db.session.commit()
			return jsonify(result),201
		return jsonify({
				'error': 'authorized',
				'message' : Author['message']
			}), 401

	if request.method == 'DELETE' and (version in ['v1']):
		Author = authorization(auth, permission=['admin'])
		print(Author)
		if Author['permission']:
			w = Writers.query.filter_by(writer_id= id).first()
			db.session.delete(w)
			db.session.commit()
			return ({
						"status":"writer delete",
						"category_id" : id
					}), 201
		return jsonify({
				'error': 'authorized',
				'message' : Author['message']
			}), 401
	return {
		'message' : 'Sorry, version not already to use'
	}, 500


@app.route("/<version>/books/", methods = ['GET', 'POST'])
def Books_func(version):
	auth = request.headers.get('Authorization')

	# mendapatkan data buku-buku
	if request.method == 'GET' and (version in ['v1']):
		Author = authorization(auth)
		if Author['permission']:
			query =  request.args
			books = Books.query.all()
			result = [ {
				"book_id"		:x.book_id,
				"title"         :x.title,
				"total_page"    :x.total_page,
				"year"          :x.year,
				"category_id"   :x.category_id,
				"writers" :[
					{
						"writer_id" : y.writer_id,
						"name"      : y.name,
						"citizen"   : y.citizen,
						"year_of_birth" : y.year_of_birth
					} for y in x.writers
				]
			}for x in books]
			if 'page' in query and query['page'] > 0:
				p = 20 * int(query['page'])
				return jsonify(result[(p-20):p]), 200
			else:
				return jsonify(result), 200
		return jsonify({
				'error': 'authorized',
				'message' : Author['message']
			}), 401

	# menambahkan data buku-buku
	if request.method == 'POST' and (version in ['v1']):
		Author = authorization(auth, permission=['admin'])
		if Author['permission']:
			data = request.get_json()
			current = []
			for_return = []
			for book in data:
				b = Books(
					book_id = 'b-'+ generate(size=10) , 
					title = book['title'],
					total_page = book['total_page'], 
					year = book['year'], 
					category_id = book['category_id']
					)
				writer_for_return = []
				for writer in book['writers']:
					if 'writer_id' in writer:
						w = Writers.query.filter_by(writer_id = writer['writer_id']).first()
						b.writers.append(w)
						writer_for_return.append({'id' : w.writer_id, 'name' : w.name})
					elif 'name' in writer:
						w = Writers(
							writer_id = 'w-' + generate(size=7), 
							name = writer['name'], 
							citizen = writer['citizen'], 
							year_of_birth = writer['year_of_birth']
							)
						b.writers.append(w)
						writer_for_return.append({
							'id' : w.writer_id, 
							'name' : w.name
						})
				current.append(b)
				for_return.append({
					'book' : b.book_id, 
					'title' : b.title,
					'writers' :writer_for_return
				})
			db.session.add_all(current)
			db.session.commit()
			return ({
				"status":"success",
				"books_added" : for_return
			}), 201
		return jsonify({
				'error': 'authorized',
				'message' : Author['message']
			}), 401
	return {
		'message' : 'Sorry, version not already to use'
	}, 500


@app.route("/<version>/book/<id>", methods = ['GET', 'PUT', 'DELETE'])
def Book_func(version, id):
	auth = request.headers.get('Authorization')

	# mendapatkan lengkap buku berdasarkan id
	if request.method == 'GET' and (version in ['v1']):
		Author = authorization(auth)
		if Author['permission']:
			b = Books.query.filter_by(book_id =  id).first()
			if not b:
				return jsonify({
					'error': 'not found',
					'message' : f"data with id {id} not found in table book"
				}),400 		
			else:
				return {
						"title"         :b.title,
						"book_id"       :b.book_id,
						"total_page"    :b.total_page,
						"year"          :b.year,
						"category_id"   :b.category_id,
						"writers" :[{
								"writer_id" : y.writer_id,
								"name"      : y.name,
								"citizen"   : y.citizen,
								"year_of_birth" : y.year_of_birth
							} for y in b.writers]
					}, 200
		return jsonify({
			'error': 'authorized',
			'message' : Author['message']
		}), 401

	# merubah data buku berdasarkan id     							//belum selesai
	# if request.method == 'PUT' and (version in ['v1']):
	# 	Author = authorization(auth, permission=['admin'])
	# 	if Author['permission']:
	# 		data = request.get_json()
	# 		b = Books.query.filter_by(book_id =  id).first()
	# 		return {
	# 				"title"         :b.title,
	# 				"book_id"       :b.book_id,
	# 				"total_page"    :b.total_page,
	# 				"year"          :b.year,
	# 				"category_id"   :b.category_id,
	# 				"writers" :[{
	# 						"writer_id" : y.writer_id,
	# 						"name"      : y.name,
	# 						"citizen"   : y.citizen,
	# 						"year_of_birth" : y.year_of_birth
	# 					} for y in b.writers]
	# 			}, 201
	# 	return jsonify({
	# 			'error': 'authorized',
	# 			'message' : Author['message']
	# 		}), 401
	
	# menghapus data buku berdasarkan id
	if request.method == 'DELETE' and (version in ['v1']):
		Author = authorization(auth, permission=['admin'])
		if Author['permission']:
			b = Books.query.filter_by(book_id =  id).first()
			if not b :
				return jsonify({
					'error': 'not found',
					'message' : f"data with id {id} not found in table book"
				}),400 		
			else:
				db.session.delete(b)
				db.session.commit()
				return ({
					"status":"book delete",
					"book_id" : id
				}), 201
		return jsonify({
				'error': 'authorized',
				'message' : Author['message']
			}), 401
	return {
		'message' : 'Sorry, version not already to use'
	}, 500




@app.route("/<version>/borrow_book", methods = ['POST'])
def Borow_book_func(version):
	auth = request.headers.get('Authorization')
	if request.method == 'POST' and (version in ['v1']):
		auth = request.headers.get('Authorization')
		Author = authorization(auth, permission='admin')
		if Author['permission']:
			data = request.get_json()
			member = Members.query.filter_by(member_id = data['member_id']).first()
			print(member) 
			t = Borow_transaction(
				trx_id = 't-'+ generate(size=10) ,
				admin = Author['id'],
				member = member.member_id,
				date = data['date']
			)
			book_for_return = []
			for book in data['books']:
				b = Books.query.filter_by(book_id = book['book_id']).first()
				bit = Book_borow(
					book_borow_id = '_-'+ generate(size=10),
					trx_id = t.trx_id,
					book_id = b.book_id,
					return_day = book['return_day'],
					date = None,
					admin = None
				)
				t.books.append(bit)
				book_for_return.append({
						'id' : b.book_id, 
						'title' : b.title
					})
			db.session.add(t)
			db.session.commit()
			return ({
				"status":"success",
				"books_added" : {

				}
			}), 201
		return jsonify({
			'error': 'authorized',
			'message' : Author['message']
		}), 401
	return {
		'message' : 'Sorry, version not already to use'
	}, 500

@app.route("/<version>/borrow_book/<id>", methods = ['GET','PUT', 'DELETE'])
def Borrow_book_id_func(version, id):
	auth = request.headers.get('Authorization')
	if request.method == 'GET' and (version in ['v1']):
		Author = authorization(auth)
		if Author['permission']:
			t = Borow_transaction.query.filter_by(trx_id = id).first()
			if (Author['user_type'] == 'MEMBER') and (Author['id'] != t.member):
				return jsonify({
					'error': 'authorized',
					'message' : "you aren't owner the data"
				}), 401
			else :
				return jsonify({
					"trx_id" : t.trx_id,
					'admin' : t.admin,
					'member' : t.member,
					'date' : str(t.date),
					'books' : [{
						'book_id' : b.book_id ,
						'date' : str(b.date),
						'return_day' : b.return_day,
						'admin' : b.admin
					} for b in t.books]
				}),200
		return jsonify({
			'error': 'authorized',
			'message' : Author['message']
		}), 401


	if request.method == 'PUT' and (version in ['v1']):
		Author = authorization(auth, permission='admin')
		if Author['permission']:
			t = Borow_transaction.query.filter_by(trx_id = id).first()
			return jsonify({
				"trx_id" : t.trx_id,
				'admin' : t.admin,
				'member' : t.member,
				'date' : t.date,
				'books' : [{
					'book_id' : b.book_id ,
					'date' : b.date,
					'return_day' : b.return_day,
					'admin' : b.admin
				} for b in t.books]
			}),200
		return jsonify({
			'error': 'authorized',
			'message' : Author['message']
		}), 401

	if request.method == 'DELETE' and (version in ['v1']):
		Author = authorization(auth, permission='admin')
		if Author['permission']:
			t = Borow_transaction.query.filter_by(trx_id = id).first()
			for b in t.books:
				db.session.delete(b)	
			db.session.delete(t)
			db.session.commit()
			return jsonify({
					"status":"book delete",
					"tansaction_borow_book_id" : id
				}),201
		return jsonify({
			'error': 'authorized',
			'message' : Author['message']
		}), 401 
	return {
		'message' : 'Sorry, version not already to use'
	}, 500 
	
@app.route("/<version>/<id_trx>/return_book/<id_book>", methods = ['PUT'])
def Return_book_func(version, id_trx, id_book):
	auth = request.headers.get('Authorization')
	if request.method == 'PUT' and (version in ['v1']):
		Author = authorization(auth, permission='admin')
		print('jalan')
		if Author['permission']:
			print('masuk')
			data = request.get_json()
			# need validator data here 
			bb = Book_borow.query.filter_by(trx_id = id_trx, book_id = id_book).first()
			bb.date = data['date']
			bb.admin = data['admin']
			db.session.commit()
			return jsonify({
				'status' : 'success',
				'message' : 'returning_book',
				'trx_id' : id_trx,
				'book_id' : id_book,
				'borrow_date' :str(bb.trx.date),
				'returning_date' : str(bb.date),
				'returning_admin' : bb.admin
			}), 201
		return jsonify({
			'error': 'authorized',
			'message' : Author['message']
		}), 401 
	return {
		'message' : 'Sorry, version not already to use'
	}, 500 
	
@app.route("/<version>/borrow_books/all")
def Borrow_book__all_func(version):
	auth = request.headers.get('Authorization')
	if request.method == 'GET' and (version in ['v1']):
		Author = authorization(auth, permission='admin')
		if Author['permission']:
			if 'late' in request.args:
				if request.args['late'] in [1, 'True', 'true', 'TRUE']:
					current_date = datetime.now().date()
					result = [{
						'member' : t.trx.name.split('-')[1],
						'books' : [{
							'book_id' : b.book_id,
							'title' : b.data.title,
							'category' : b.data.category.name,
							'late' : ((current_date - t.date).days if b.date is None else (b.date - t.date).days),
							'date_borrow' : str(t.date),
							'date_return' : b.date if b.date is None else str(b.date),
							'return_day' : b.return_day
						} for b in t.books if b.return_day <  ((current_date - t.date).days if b.date is None else (b.date - t.date).days) ]
					}for t in Borow_transaction.query.all()]	
					result = [x for x in result if len(x['books']) > 0]
				if request.args['late'] in [0 , 'False', 'false', 'FALSE']:
					current_date = datetime.now().date()
					result = [{
						'member' : t.trx.name.split('-')[1],
						'books' : [{
							'book_id' : b.book_id,
							'title' : b.data.title,
							'category' : b.data.category.name,
							'actual_return_day' : ((current_date - t.date).days if b.date is None else (b.date - t.date).days),
							'date_borrow' : str(t.date),
							'date_return' : b.date if b.date is None else str(b.date),
							'return_day' : b.return_day
						} for b in t.books if b.return_day >= ((current_date - t.date).days if b.date is None else (b.date - t.date).days)]
					}for t in Borow_transaction.query.all()]	
					result = [x for x in result if len(x['books']) > 0]	
			else :
				result = [{
					"trx_id" : t.trx_id,
					'admin' : t.admin,
					'member' : t.member,
					'date' : str(t.date),
					'books' : [{
						'book_id' : b.book_id,
						'title' : b.data.title,
						'total_page' : b.data.total_page,
						'year' : b.data.year,
						'category' : b.data.category.name,
						"writers" :[y.name for y in b.data.writers],
						'date' : b.date,
						'return_day' : b.return_day,
						'admin' : b.admin
					} for b in t.books]
				}for t in Borow_transaction.query.all()]
			return result,200
		return jsonify({
			'error': 'authorized',
			'message' : Author['message']
		}), 401


@app.route("/<version>/borrow_books/date_range")
def Borrow_book_range_func(version):
	auth = request.headers.get('Authorization')
	if request.method == 'GET' and (version in ['v1']):
		Author = authorization(auth, permission='admin')
		if Author['permission']:
			range = request.get_json()
			if ('from' in range) or ('until' in range):
				if 'late' in request.args:
					if request.args['late'] in [1, 'True', 'true', 'TRUE']:
						current_date = datetime.now().date()
						result = [{
							'member' : t.trx.name.split('-')[1],
							'books' : [{
								'book_id' : b.book_id,
								'title' : b.data.title,
								'category' : b.data.category.name,
								'late' : ((current_date - t.date).days if b.date is None else (b.date - t.date).days) - b.return_day,
								'date_borrow' : str(t.date),
								'date_return' : b.date if b.date is None else str(b.date),
								'return_day' : b.return_day
							} for b in t.books if b.return_day < ((current_date - t.date).days if b.date is None else (b.date - t.date).days) ]
						}for t in Borow_transaction.query.filter(Borow_transaction.date >= range['from'], Borow_transaction.date <= range['until']).all()]	
						result = [x for x in result if len(x['books']) > 0]
					if request.args['late'] in [0 , 'False', 'false', 'FALSE']:
						current_date = datetime.now().date()
						result = [{
							'member' : t.trx.name.split('-')[1],
							'books' : [{
								'book_id' : b.book_id,
								'title' : b.data.title,
								'category' : b.data.category.name,
								'actual_return_day' : ((current_date - t.date).days if b.date is None else (b.date - t.date).days),
								'date_borrow' : str(t.date),
								'date_return' : b.date if b.date is None else str(b.date),
								'return_day' : b.return_day
							} for b in t.books if b.return_day >= ((current_date - t.date).days if b.date is None else (b.date - t.date).days) ]
						}for t in Borow_transaction.query.filter(Borow_transaction.date >= range['from'], Borow_transaction.date <= range['until']).all()]	
						result = [x for x in result if len(x['books']) > 0]
				else :
					result = [{
						"trx_id" : t.trx_id,
						'admin' : t.admin,
						'member' : t.member,
						'date' : str(t.date),
						'books' : [{
							'book_id' : b.book_id,
							'title' : b.data.title,
							'total_page' : b.data.total_page,
							'year' : b.data.year,
							'category' : b.data.category.name,
							"writers" :[y.name for y in b.data.writers],
							'date' : b.date,
							'return_day' : b.return_day,
							'admin' : b.admin
						} for b in t.books]
					}for t in Borow_transaction.query.filter(Borow_transaction.date >= range['from'], Borow_transaction.date <= range['until']).all()]
				return result,200
			else :
				return 'payload failed, that need json object with key "from" and "until".', 401
		return jsonify({
			'error': 'authorized',
			'message' : Author['message']
		}), 401





if __name__ == '__main__':
	app.run()