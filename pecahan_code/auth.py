from base64 import b64decode, b64encode
from .models import Members, Administrators

def authorization(token, permission = ['admin', 'member']):
        token = token.split(' ')
        if len(token)==2 and token[0]=='Basic':
            A = b64decode(token[1]).decode('utf-8').split(':')
            if len(A) == 2:
                author = None
                pw = convert_authorization_password(A[1])
                if (A[0][:6] == 'Admin-') and ('admin' in permission):
                    author = Administrators.query.filter_by(name = A[0], password = pw).first()
                    if author:
                        return {
                            'permission'    : True,
                            'id'            : author.admin_id, 
                            'username'      : author.name,
                            'user_type'     : 'ADMIN'
                        }
                elif (A[0][:7] == 'Member-') and ('member' in permission):
                    author = Members.query.filter_by(name = A[0], password = pw).first()
                    if author:
                        return {
                            'permission'    : True,
                            'id'            : author.member_id, 
                            'username'      : author.name,
                            'user_type'     : 'MEMBER'

                        }
                return {
                    'permission' : False,
                    'message' : 'bad users and password'
                }
        return {
            'permission' : False,
            'message' : 'bad authentication'
        }
    

def convert_authorization_password(password):
    return b64encode(password.encode('utf-8')).decode('utf-8')



