from flask import Flask
from sqlalchemy import UniqueConstraint
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
app.config['SECRET_KEY']='secret'
app.config['SQLALCHEMY_DATABASE_URI']='postgresql://postgres:Pgher1@localhost/library4'
db = SQLAlchemy(app)

migrate = Migrate(app, db)

class Categories(db.Model):
    category_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50), nullable=False, unique=True)
    description = db.Column(db.String, nullable=False)
    books = db.relationship('Books', backref='category', lazy='dynamic')
    
class Writers(db.Model):
    writer_id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String(50), nullable=False, unique=True)
    citizen = db.Column(db.String(50))
    year_of_birth = db.Column(db.Integer)

class Books(db.Model):
    book_id = db.Column(db.String, primary_key=True)
    title = db.Column(db.String(225), nullable=False)
    total_page = db.Column(db.Integer)
    year = db.Column(db.Integer, nullable=False)
    category_id = db.Column(db.Integer, db.ForeignKey('categories.category_id'), nullable=False)
    writers = db.relationship('Writers', secondary='books_writer', lazy='subquery', backref=db.backref('books', lazy=True))
    borrow = db.relationship('Book_borow', backref='data', lazy='dynamic')

books_writer = db.Table(
    'books_writer',
    db.Column('writer_id', db.String, db.ForeignKey('writers.writer_id'), nullable=False),
    db.Column('book_id', db.String, db.ForeignKey('books.book_id'), nullable=False),
    UniqueConstraint('writer_id', 'book_id', name='uix_writer_book')
)
    
class Members(db.Model):
    member_id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String, nullable=False)
    trx = db.relationship('Borow_transaction', backref='trx', lazy='dynamic')

class Administrators(db.Model):
    admin_id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String, nullable=False)
    trx_in = db.relationship('Borow_transaction', backref='trx_in', lazy='dynamic')
    trx_out = db.relationship('Book_borow', backref='trx_out', lazy='dynamic')

class Borow_transaction(db.Model):
    trx_id = db.Column(db.String, primary_key=True)
    admin = db.Column(db.String, db.ForeignKey('administrators.admin_id'), nullable=False)
    member = db.Column(db.String, db.ForeignKey('members.member_id'), nullable=False)
    date = db.Column(db.Date, nullable=False)
    books = db.relationship('Book_borow', backref='trx', lazy='dynamic')

class Book_borow(db.Model):
    book_borow_id = db.Column(db.String, primary_key=True)
    trx_id = db.Column(db.String, db.ForeignKey('borow_transaction.trx_id'), nullable=False)
    book_id = db.Column(db.String, db.ForeignKey('books.book_id'), nullable=False )
    return_day = db.Column(db.Integer)
    date = db.Column(db.Date)
    admin = db.Column(db.String, db.ForeignKey('administrators.admin_id'))
    __table_args__ = (
        UniqueConstraint('trx_id', 'book_id'),
    )
    